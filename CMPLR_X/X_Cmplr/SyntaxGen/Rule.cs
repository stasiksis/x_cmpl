﻿using System;
using System.Collections.Generic;

namespace NS_Rules
{
	public class Rule
	{
		public string name;
		public List<List<string>> vars;
	}

	public class NewRule
	{
		public short name;
		public List<List<short>> vars;
	}
}

