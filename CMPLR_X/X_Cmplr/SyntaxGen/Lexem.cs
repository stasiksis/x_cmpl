﻿using System;

namespace NS_Lexems
{
	public enum Lexem
	{
		/*(0)       idents  */
		IDENT = 0,
		/*(1-3)     values  */
		INT_CONST = 1,
		FLOAT_CONST = 2,
		STRING_LITERAL = 3,
		/*(4-11)    types   */
		CHAR = 4,
		DOUBLE = 5,
		FLOAT = 6,
		INT = 7,
		LONG = 8,
		SHORT = 9,
		VOID = 10,
		BYTE = 11,
		/*(12-14)   prefix  */
		CONST = 12,
		SIGNED = 13,
		UNSIGNED = 14,
		/*(15-16)   if  */
		IF = 15,
		ELSE = 16,
		/*(17-19)   switch  */
		SWITCH = 17,
		CASE = 18,
		DEFAULT = 19,
		/*(20-24)   loop  */
		FOR = 20,
		DO = 21,
		WHILE = 22,
		BREAK = 23,
		CONTINUE = 24,
		/*(25-27)   ( ) return */
		FUNC_ARG_LEFT = 25,
		FUNC_ARG_RIGHT = 26,
		RETURN = 27,
		/*(28-32)   + - / * % */
		OP_ADD = 28,
		OP_SUB = 29,
		OP_DIV = 30,
		OP_MUL = 31,
		OP_PROCENT = 32,
		/*(33-38)   = +=... */
		ASSIGN = 33,
		ASSIGN_ADD = 34,
		ASSIGN_SUB = 35,
		ASSIGN_MUL = 36,
		ASSIGN_DIV = 37,
		ASSIGN_PROCENT = 38,
		/*(39-43)   &=... */
		ASSIGN_AND = 39,
		ASSIGN_OR = 40,
		ASSIGN_XOR = 41,
		ASSIGN_RIGHT = 42,
		ASSIGN_LEFT = 43,
		/*(44-45)   ++, -- */
		INC = 44,
		DEC = 45,
		/*(46-51)   &, |, ^, >>, << !*/
		OP_AND = 46,
		OP_OR = 47,
		OP_XOR = 48,
		OP_RIGHT = 49,
		OP_LEFT = 50,
		OP_NOT = 51,
		/*(52-59)   ==,!=,>=,<=,&&,||, <, >*/
		LOGIC_EQ = 52,
		LOGIC_NOT_EQ = 53,
		LOGIC_NOT_S = 54,
		LOGIC_NOT_L = 55,
		LOGIC_AND = 56,
		LOGIC_OR = 57,
		LOGIC_S = 58,
		LOGIC_L = 59,
		/*(60-62)   [], -> */
		PTR_MASS_LEFT = 60,
		PTR_MASS_RIGHT = 61,
		PTR_LINK = 62,
		/*(63-68)   Utils { } ; , . : */
		BLOCK_LEFT = 63,
		BLOCK_RIGHT = 64,
		SEMICOLON = 65,
		COMMA = 66,
		POINT = 67,
		DOUBLE_POINT = 68,
		/*(69-71)   Sub Lexem */
		START = 69,
		ERROR = 70,
		END = 71,
		TYPEDEF = 72,
		NEW_TYPE = 73
	}
}

