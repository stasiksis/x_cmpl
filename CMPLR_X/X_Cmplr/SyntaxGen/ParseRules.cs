﻿using System;
using System.Collections.Generic;
using NS_Rules;
using System.Linq;
using NS_Lexems;

namespace NS_ParseRules
{
	public class ParseRules
	{
		string grammar;
		public ParseRules (String grammar)
		{
			this.grammar = grammar;
		}

		public void parseRules(out List<Rule> rules, out List<NewRule> newRules, out Dictionary<string, short> allSymbols,
			out Dictionary<short, List<Lexem>> first, out Dictionary<short, List<Lexem>> follow, out String startRule, String startName)
		{

			allSymbols = new Dictionary<string, short>();
			first = new Dictionary<short, List<Lexem>>();
			follow = new Dictionary<short, List<Lexem>>();
			newRules = new List<NewRule>();
			rules = new List<Rule>();

			grammar = grammar.Substring(grammar.IndexOf('\n') + 1);
			// Парсим список правил
			Rule current = null;
			List<string> lineRule = null;

			foreach (var word in grammar.Split(new char[] { '\n', '\r', ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries))
			{
				switch (word)
				{
				case ":":
					lineRule = new List<string>();
					break;
				case "|":
					current.vars.Add(lineRule);
					lineRule = new List<string>();
					break;
				case ";":
					current.vars.Add(lineRule);
					rules.Add(current);
					current = null;
					break;
				default:
					if (current == null)
						current = new Rule() { vars = new List<List<string>>(), name = word };
					else
						lineRule.Add(word);
					break;
				}
			}

			if (rules.Count == 0)
				throw new ArgumentException("В грамматике нет правил");
			else
				startRule = rules[0].name;
			rules.Add(new Rule()
				{
					name = startName,
					vars = new List<List<string>>() {
						new List<string>() { startRule }
					}
				});
			var symbols = Enum.GetNames(typeof(Lexem)).Union(rules.Select(x => x.name));
			short i = 0;
			foreach (string t in symbols)
			{
				allSymbols.Add(t, i);
				i++;
			}

			for (i = 0; i < rules.Count; i++)
			{
				List<List<short>> vars = new List<List<short>>();
				for (short j = 0; j < rules[i].vars.Count; j++)
				{
					List<short> vars1 = new List<short>();
					for (short k = 0; k < rules[i].vars[j].Count; k++)
					{
						vars1.Add(allSymbols[rules[i].vars[j][k]]);
					}
					vars.Add(vars1);
				}

				newRules.Add(new NewRule
					{
						name = allSymbols[rules[i].name],
						vars = vars
					});
			}

			first[allSymbols[startName]] = new List<Lexem>() { Lexem.START };
			follow[allSymbols[startName]] = new List<Lexem>() { Lexem.END };
		}
	}
}

