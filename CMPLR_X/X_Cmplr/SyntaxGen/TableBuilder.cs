﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NS_Rules;
using NS_Lexems;
using NS_Utils;

namespace NS_SyntaxTableBuilder
{
	public class TableBuilder
	{
		public Dictionary<string, short> allSymbols = new Dictionary<string, short>();
        private Dictionary<short, List<Lexem>> first = new Dictionary<short, List<Lexem>>();
        private Dictionary<short, List<Lexem>> follow = new Dictionary<short, List<Lexem>>();
        public List<NewRule> newRules = new List<NewRule>();
		string grammar;
		readonly string startName = "::Start::";

		string startRule = String.Empty;
		Lexem[] terms = (Lexem[])Enum.GetValues(typeof(Lexem));
		List<Rule> rules = new List<Rule>();

		Tuple<Actions, short, LRItem>[,] tableAction;
		short[,] tableGoto;

		string GrammarName = String.Empty;

		/// <summary>
		/// Токены берутся из enum Lexem.
		/// </summary>
		/// <param name="grammar">Входная грамматика. Первое правило - начальное правило! Первая строка: %grammarName</param>
		public TableBuilder(string grammar)
		{
			this.grammar = grammar;

			GrammarName = grammar.Split(new char[] { '\n', '\r', ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries)[0].Substring(1);

			try 
			{
				Utils.loadTables(GrammarName, out rules, out allSymbols, out startRule, out tableAction, out tableGoto);
				return;
			}
			catch(Exception e)
			{
				if (!(e is DirectoryNotFoundException || e is IOException))
					throw;
			}

			Console.WriteLine("Grammar "+ GrammarName+" does not exist. Initialize rools.");

			NS_ParseRules.ParseRules parseRules = new NS_ParseRules.ParseRules(grammar);
			parseRules.parseRules(out rules, out newRules, out allSymbols, out first, out follow, out startRule, startName);
		}

		private List<LRItem> Closure(List<LRItem> result)
		{
			//var result = new List<LRItem>(mass); // I

			for (short i = 0; i < result.Count; i++)
			{
				//для каждой ситуации [A -> alpha .B beta, a] из result
				var item = result[i];
				if (item.cursor >= item.var.Count || !newRules.Exists(x => x.name.Equals(item.var[item.cursor])))
					continue;

				// для каждого правила вывода B -> gamma из rules
				newRules.Find(x => x.name.Equals(item.var[item.cursor])).vars.ForEach(var =>
					{
						var subQuery = new List<short>();
						for (short j = (short)(item.cursor + 1); j < item.var.Count; j++)
							subQuery.Add(item.var[j]);

						subQuery.Add(item.prefix);

						// для каждого терминала b из FIRST(beta a)
						FindFirst(subQuery).ForEach(terminal =>
							{
								// если [B ->.gamma, b] нет в result
								if (!result.Exists(x =>
									x.prefix == (short)terminal
									&& x.cursor == 0
									&& x.var.Equals(var)
									&& x.holder.Equals(item.var[item.cursor])
								))
								{
									result.Add(new LRItem()
										{
											holder = item.var[item.cursor],
											cursor = 0,
											var = var,
											prefix = (short)terminal
										});
								}

							});
					});
			}

			return result;
		}

		private List<LRItem> Goto(List<LRItem> mass, string symbol)
		{

			return Closure(
				mass.Where(x => 
					x.cursor < x.var.Count 
					&& x.var[x.cursor].Equals(allSymbols[symbol]))
				.Select(x => x.IncCursor()).ToList()
			);
		}

		private List<List<LRItem>> Items()
		{
            short i = 0;
            var symbols = Enum.GetNames(typeof(Lexem)).Union(rules.Select(x => x.name));
            if(allSymbols.Count == 0)
            {
                i = 0;
                foreach (string t in symbols)
                {
                    allSymbols.Add(t, i);
                    i++;
                }
            }
            var G = new List<LRItem>();
			List<short> help = new List<short>();
            for (i = 0; i < rules.Last().vars[0].Count; i++)
                help.Add(allSymbols[rules.Last().vars[0][i]]);

            G.Add(new LRItem()
            {
                cursor = 0,
                prefix = (short)Lexem.END,
                holder = allSymbols[rules.Last().name],
                var = help
            });
            
            var result = new List<List<LRItem>>();
			result.Add(Closure(G));
			for (i = 0; i < result.Count(); i++)
			{
				var vertex = result[i];
                foreach(var elem in symbols)
                {
                    var got = Goto(vertex, elem);
                    if (got.Count != 0 && !result.Exists(x => CompareStates(ref x, ref got)))
                    {
                        result.Add(got);
                    }
                }
			}
            
            return result;
		}

		private bool CompareStates(ref List<LRItem> state1, ref List<LRItem> state2)
		{
			if (state1.Count != state2.Count)
				return false;
            int len = state2.Count;
            for (int i = 0; i < len; i++)
                if (!state1.Contains(state2[i]))
                    return false;

            return true;
        }

		private bool Follow(NewRule rule)
		{
			bool loop = false;

			newRules.ForEach(x =>
				{
					foreach (List<short> var in x.vars.Where(var => var.Contains(rule.name)))
					{
						short next = (short)(var.IndexOf(rule.name) + 1);
						Lexem lexem;

						if (next == var.Count)
						{
							foreach (Lexem item in follow[x.name])
							{
								if (!follow[rule.name].Contains(item))
								{
                                    follow[rule.name].Add(item);
									loop = true;
								}
							}
						}
						else if (Enum.TryParse(allSymbols.ElementAt(var[next]).Key, out lexem))
						{
							if (!follow[rule.name].Contains(lexem))
							{
                                follow[rule.name].Add(lexem);
								loop = true;
							}
						}
						else
						{
							NewRule A = newRules.Find(i => i.name.Equals(var[next]));
							if (A == null)
								throw new Exception("name: " + var[next]);
							foreach (Lexem item in first[A.name])
							{
								if (item == Lexem.ERROR)
								{
									foreach (Lexem item1 in follow[A.name])
									{
										if (!follow[A.name].Contains(item1))
										{
                                            follow[rule.name].Add(item1);
											loop = true;
										}
									}
								}
								else if (!follow[rule.name].Contains(item))
								{
                                    follow[rule.name].Add(item);
									loop = true;
								}
							}
						}
					}
				});

			return loop;
		}

		private List<Lexem> FindFirst(List<short> var)
		{
			List<Lexem> result;
			Lexem lexem;

			if (Enum.TryParse(allSymbols.Keys.ElementAt(var[0]), out lexem))
			{
				result = new List<Lexem>();
				result.Add(lexem);
			}
			else
			{
                //result = newRules.Find(x => x.name == var[0]).first;
                //result = first[allSymbols.ElementAt(newRules.Find(x => x.name == var[0]).name).Key];
                result = first[newRules.Find(x => x.name == var[0]).name];
                if (result.Contains(Lexem.ERROR))
					result.AddRange(
						FindFirst(
							var.GetRange(1, var.Count - 1)
						)
					);
			}

			return result;
		}

		private List<Lexem> First(NewRule rule)
		{
			if (rule == null)
				return null;
            if (first[rule.name] != null)
                first[rule.name] = new List<Lexem>();
		    var result = first[rule.name];

			rule.vars.ForEach(var =>
				{
					if (var.Count == 0)
					{
						//Добавление epsilon к списку first
						if (result.IndexOf(Lexem.ERROR) == -1) result.Add(Lexem.ERROR);
					}
					else
					{
						bool IsEmpty = false;
						foreach (short item in var)
						{
							if (item.Equals(rule.name))
								break;

							Lexem lexem;
							if (Enum.TryParse<Lexem>(allSymbols.ElementAt(item).Key, out lexem))
							{
								if (result.IndexOf(lexem) == -1)
									result.Add(lexem);
								break;
							}
							var newRule = newRules.Find(x => x.name.Equals(item));
							if (newRule == null)
								throw new ArgumentException("Не существует токена " + item +
									" в правиле " + rule.name);
							var newF = first[newRule.name].Count == 0 ? First(newRule) : first[newRule.name];

							if (newF == null)
								throw new Exception("Unknow name: " + item);
							foreach (Lexem l in newF)
							{
								if (!result.Contains(l))
									result.Add(l);
								else if (l == Lexem.ERROR)
									IsEmpty = true;
							}

							if (!IsEmpty)
								break;
						}
					}

				});

			return result;
		}

        private void fillingTables(List<List<LRItem>> Q, out Tuple<Actions, short, LRItem>[,] action, out short[,] GOTO)
        {
            var lexems = Enum.GetNames(typeof(Lexem));
            int len = Q.Count;
            int lenLexems = lexems.Count();

            action = new Tuple<Actions, short, LRItem>[len, lenLexems];
            GOTO = new short[len, rules.Count];

            for (short i = 0; i < len; i++)
            {
                var q = Q[i];
                for (short j = 0; j < lenLexems; j++)
                {
                    Lexem lexem = (Lexem)Enum.Parse(typeof(Lexem), lexems[j]);
                    var a = allSymbols[lexems[j]];
                    List<LRItem> nextQ = Goto(q, allSymbols.Keys.ElementAt(a));
                    for (short k = 0; k < q.Count; k++)
                    {
                        if (q[k].cursor != q[k].var.Count && q[k].var[q[k].cursor] == a)
                        {
                            action[i, j] = new Tuple<Actions, short, LRItem>(Actions.SHIFT, (short)Q.FindIndex(x => CompareStates(ref nextQ, ref x)), q[k]);
                        }
                    }

                    for(short k = 0; k < q.Count; k++)
                    {
                        if(q[k].cursor == q[k].var.Count && q[k].prefix == (short)lexem)
                        {
                            var newAction = new Tuple<Actions, short, LRItem>(Actions.REDUCE, -1, q[k]);
                            action[i, j] = newAction;
                        }
                    }
                    
                    if (q.Exists(item => item.holder.Equals(allSymbols[startName]) && item.cursor == item.var.Count))
                        action[i, (short)Lexem.END] = new Tuple<Actions, short, LRItem>(Actions.ACCEPT, -1, null);
                }
                for (short j = 0; j < rules.Count; j++)
                {
                    var A = newRules[j].name;
                    var next = q.Find(item => item.var.Count != item.cursor && item.var[item.cursor].Equals(A));
                    if (next != null)
                    {
                        var nextQ = Goto(q, allSymbols.Keys.ElementAt(A));
                        short index = (short)Q.FindIndex(x => CompareStates(ref nextQ, ref x));
                        if (index == -1)
                        {
                            throw new IndexOutOfRangeException();
                        }

                        GOTO[i, j] = index;
                    }
                    else
                        GOTO[i, j] = -1;
                }
            }
        }

        /// <summary>
		/// Строит таблицы
		/// </summary>
		/// <param name="tableAction">перенос.свёртка.допуск.ошибка</param>
		/// <param name="tableGoto">short - номер, куда состоится переход</param>
		public void createTable(out List<Rule> rules,
			out Tuple<Actions, short, LRItem>[,] tableAction,
			out short[,] tableGoto)
		{
			if (this.tableAction != null)
			{
				rules = this.rules;
				tableAction = this.tableAction;
				tableGoto = this.tableGoto;
				return;
			}

            Console.WriteLine("Start to build state machine...");

            DateTime ThToday = DateTime.Now;
			string ThData = ThToday.ToString();
			Console.WriteLine (ThData);
			
            //First & Follow
			rules = this.rules;

            newRules.ForEach(rule => first[rule.name] = new List<Lexem>());
            newRules.ForEach(rule => follow[rule.name] = new List<Lexem>());
            newRules.ForEach(rule => first[rule.name] = First(rule));
			bool loop = true;
			while (loop)
			{
				loop = false;
				newRules.ForEach(rule => { loop |= Follow(rule); });
			}
			
            //////////////////////////////////////////////////////////////
			var Q = Items();
            var lexems = Enum.GetNames(typeof(Lexem));

            var action = new Tuple<Actions, short, LRItem>[Q.Count, lexems.Count()];
            var GOTO = new short[Q.Count, rules.Count];
            Console.WriteLine("State machine builded!\nStart to build tables...");

            ThToday = DateTime.Now;
			ThData = ThToday.ToString();
			Console.WriteLine (ThData);

            fillingTables(Q, out action, out GOTO);

            ThToday = DateTime.Now;
			ThData = ThToday.ToString();
			Console.WriteLine (ThData);

            tableAction = new Tuple<Actions, short, LRItem>[Q.Count, lexems.Count()];
			tableGoto = new short[Q.Count, rules.Count];
			for (short i = 0; i < Q.Count; i++)
			{
				for (short j = 0; j < lexems.Count(); j++ )
					tableAction[i,j] = action[i, j];

				for (short j = 0; j < rules.Count; j++ )
					tableGoto[i, j] = GOTO[i, j];
			}

			Console.WriteLine("Tables builded. Save them:)");
			Utils.writeTablesToFile(action, GOTO, GrammarName, rules);
		}

	}

	/*
     *  UTILS
     */
	public enum Actions { SHIFT, REDUCE, ACCEPT, ERROR }

	[Serializable()]
	public class LRItem : IEquatable<LRItem>
	{
		public short holder;
		public List<short> var;
		public short cursor;
		public short prefix;

		public bool Equals(LRItem other)
		{
            if (other == null)
                return false;

			if (prefix != other.prefix)
                return false;

		    return
				holder.Equals(other.holder)
				&& cursor == other.cursor
				&& var == other.var;
		}

        public override int GetHashCode()
        {
            return holder ^ cursor;
        }

        public override string ToString()
		{
			var result =  holder + " ->";
			for(short i = 0; i < var.Count;i++)
				result += (i == cursor ? " * " : " ") + var[i];
			if (cursor == var.Count)
				result += " *";
			return result + ", " + prefix ;
		}

		public LRItem IncCursor()
		{
			return new LRItem()
			{
				holder = holder,
				var = var,
				cursor = (short)(cursor + 1),
				prefix = prefix
			};
		}
	}
}
