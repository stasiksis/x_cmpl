﻿using System;
using System.Collections.Generic;
using NS_SyntaxTableBuilder;
using System.Linq;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NS_Rules;
using NS_Lexems;

namespace NS_Utils
{
	public class Utils
	{
		public static void writeTablesToFile(Tuple<Actions, short, LRItem>[,] action, short[,] GOTO, String GrammarName, List<Rule> rules)
		{
			Directory.CreateDirectory("Grammar/" + GrammarName);

			var writeStream = File.CreateText("Grammar/" + GrammarName + "/" + GrammarName + ".action");
			writeStream.Write(JsonConvert.SerializeObject(action));
			writeStream.Close();
			writeStream = File.CreateText("Grammar/" + GrammarName + "/" + GrammarName + ".goto");
			writeStream.Write(JsonConvert.SerializeObject(GOTO));
			writeStream.Close();
			writeStream = File.CreateText("Grammar/" + GrammarName + "/" + GrammarName + ".rules");
			writeStream.Write(JsonConvert.SerializeObject(rules));
			writeStream.Close();

			Console.WriteLine("All tables Saved!");
		}

		public static void loadTables(String GrammarName, out List<Rule> rules, out Dictionary<string, short> allSymbols,
			out String startRule, out Tuple<Actions, short, LRItem>[,] tableAction, out short[,] tableGoto)
		{
			allSymbols = new Dictionary<string, short>();
			rules = new List<Rule>();

			tableAction = null;
			tableGoto = null;

			var grammarTables = Directory.GetFiles("Grammar/" + GrammarName + "/");

			if (grammarTables.Count() != 3)
				throw new IOException();
			Array.Sort(grammarTables);
			Console.WriteLine("Grammar " + GrammarName + " exists. Start to load tables...");

			foreach (var tableName in grammarTables)
			{
				switch (tableName.Substring(tableName.LastIndexOf('.')))
				{
				case ".action":
					Console.Write("Loading action table...");
					tableAction = JsonConvert.DeserializeObject<Tuple<Actions, short, LRItem>[,]>(File.ReadAllText(tableName));
					Console.WriteLine("finished.");
					break;
				case ".goto":
					Console.Write("Loading goto table...");
					tableGoto = JsonConvert.DeserializeObject<short[,]>(File.ReadAllText(tableName));
					Console.WriteLine("finished.");
					break;
				case ".rules":
					Console.Write("Loading rules...");
					rules = JsonConvert.DeserializeObject<Rule[]>(File.ReadAllText(tableName)).ToList();
					Console.WriteLine("finished.");
					break;
				}

			}

			startRule = rules[0].name;
			var symbols = Enum.GetNames(typeof(Lexem)).Union(rules.Select(x => x.name));
			short i = 0;
			foreach (string t in symbols)
			{
				allSymbols.Add(t, i);
				i++;
			}
			Console.WriteLine("Grammar loading finished!");
		}
	}
}

