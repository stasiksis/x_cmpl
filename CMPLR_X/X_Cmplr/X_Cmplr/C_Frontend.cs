﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using NS_Hyperon;
using NS_XCmplr;
using NS_SyntaxTableBuilder;
using NS_Compiler;
using NM_DirectedGraphLibrary.NM_DirectedGraphLibraryInternals;
using NS_Lexems;
using NS_Rules;

namespace NS_C_Frontend
{

    class Message
    {
        public bool IsError;
        public string Text;

        public override string ToString()
        {
            return (IsError ? "ERROR: " : "WARNING: ") + Text;
        }
    }

    class Fragment
    {
        public Position Start, End = null;

        public override string ToString()
        {
            return Start.ToString() + (End != null ? "-" + End.ToString() : "");
        }
    }

    class Position : IComparable<Position>
    {
        string program = String.Empty;
        public int Index { get; private set; }
        public int Line { get; private set; }
        public int Pos { get; private set; }
        public int Current
        {
            get
            {
                return (Index == program.Length) ? -1
                    : Char.ConvertToUtf32(program, Index);
            }
        }
        public bool IsNewLine
        {
            get
            {
                if (Index == program.Length) return true;
                if (program[Index] == '\r' && Index + 1 < program.Length)
                    return program[Index + 1] == '\n';

                return program[Index] == '\n';
            }
        }

        public bool IsWhiteSpace
        {
            get
            {
                return Index != program.Length && Char.IsWhiteSpace(program, Index);
            }
        }

        public bool IsLetter
        {
            get
            {
                return Index != program.Length && (Char.IsLetter(program, Index) || Current == '_');
            }
        }

        public bool IsLetterOrDigit
        {
            get
            {
                return Index != program.Length && (Char.IsLetterOrDigit(program, Index) || Current == '_');
            }
        }

        public bool IsDecimalDigit
        {
            get
            {
                return Index != program.Length && program[Index] >= '0' && program[Index] <= '9';
            }
        }

        private Position() { }

        public Position(string program)
        {
            this.program = program;
            Line = Pos = 1;
            Index = 0;
        }

        public Position Copy()
        {
            Position result = new Position()
            {
                Line = this.Line,
                Pos = this.Pos,
                Index = this.Index,
                program = this.program
            };
            return result;
        }

        public override string ToString()
        { return "(" + Line + "," + Pos + ")"; }

        public static Position operator ++(Position p)
        {
            if (p.Index < p.program.Length)
            {
                if (p.IsNewLine)
                {
                    if (p.program[p.Index] == '\r') p.Index++;
                    p.Line++;
                    p.Pos = 1;
                }
                else if (p.program[p.Index] == '\t')
                {
                    if (Char.IsHighSurrogate(p.program[p.Index])) p.Index++;
                    p.Pos += 4;
                }
                else
                {
                    if (Char.IsHighSurrogate(p.program[p.Index])) p.Index++;
                    p.Pos++;
                }
                p.Index++;
            }
            return p;
        }

        public int CompareTo(Position other)
        { return Index.CompareTo(other.Index); }
    }

    abstract class Token
    {
        public Lexem Tag { get; set; }
        public Fragment Coords { get; set; }

        public override String ToString()
        { return Tag + ":" + Coords; }
    }

    class IdentToken : Token
    {
        public int Code { get; set; }
        public IdentToken() : base() { Tag = Lexem.IDENT; }

        public override String ToString()
        {
            return base.ToString().Split(':')[0]
                    + "[" + Code + "]:" +
                    base.ToString().Split(':')[1];
        }
    }

    class INumberToken : Token
    {
        public long Value { get; set; }
        public INumberToken() : base() { Tag = Lexem.INT_CONST; }

        public override String ToString()
        {
            return base.ToString().Split(':')[0]
                    + "[" + Value + "]:" +
                    base.ToString().Split(':')[1];
        }
    }

    class FNumberToken : Token
    {
        public double Value { get; set; }
        public FNumberToken() : base() { Tag = Lexem.FLOAT_CONST; }

        public override String ToString()
        {
            return base.ToString().Split(':')[0]
                    + "[" + Value + "]:" +
                    base.ToString().Split(':')[1];
        }
    }

    class StringToken : Token
    {
        public string Value { get; set; }
        public StringToken() : base() { Tag = Lexem.STRING_LITERAL; }

        public override String ToString()
        {
            return base.ToString().Split(':')[0]
                    + "[" + Value + "]:" +
                    base.ToString().Split(':')[1];
        }
    }

    class SpecToken : Token { }

    class Lexer
    {
        string program = String.Empty;
        COptions opts;
        Compiler compiler;
		public List<Fragment> comments = new List<Fragment>();
      
        bool afterIdent = false;
        bool afterValue = false;
        Token bufToken = null;
        bool typedef = false;
        List<String> listTypedef = new List<String>();
        #region Сканеры
        private Token scan(Position cursor)
        {
            Token result = null;
            if (bufToken != null)
            {
                result = bufToken;
                bufToken = null;
                return result;
            }

            while (cursor.IsWhiteSpace)         //WhiteSpaces
                cursor++;

            if (cursor.Current == -1)
                return new SpecToken()
                {
                    Tag = Lexem.END,
                    Coords = new Fragment() { Start = cursor.Copy() }
                };

            if (cursor.IsLetter)                //Idents and keys
                result = FindIdent(cursor);
            else if (cursor.IsDecimalDigit)     //Numbers
                result = FindNumber(cursor);
            else if (cursor.Current == '\"')    //Strings
                result = FindStr(cursor);
            else if (cursor.Current == '\'')    //Chars
                result = FindChar(cursor);
            else                                //Other
                result = FindOther(cursor);


            return result ?? scan(cursor);
        }

        private Lexem getLexemByValue(String strProgram)
        {
            switch(strProgram)
            {
                case "int_const": return Lexem.INT_CONST;
                case "float_const": return Lexem.FLOAT_CONST;
                case "char": return Lexem.CHAR;
                case "double": return Lexem.DOUBLE;
                case "float": return Lexem.FLOAT;
                case "int": return Lexem.INT;
                case "long": return Lexem.LONG;
                case "short": return Lexem.SHORT;
                case "void": return Lexem.VOID;
                case "byte": return Lexem.BYTE;
                case "const": return Lexem.CONST;
                case "signed": return Lexem.SIGNED;
                case "unsigned": return Lexem.UNSIGNED;
                case "if": return Lexem.IF;
                case "else": return Lexem.ELSE;
                case "switch": return Lexem.SWITCH;
                case "case": return Lexem.CASE;
                case "default": return Lexem.DEFAULT;
                case "for": return Lexem.FOR;
                case "while": return Lexem.WHILE;
                case "break": return Lexem.BREAK;
                case "continue": return Lexem.CONTINUE;
                case "typedef": return Lexem.TYPEDEF;
				case "return": return Lexem.RETURN;
            }
            if (listTypedef.Contains(strProgram))
                return Lexem.NEW_TYPE;

            return Lexem.IDENT;
        }

        private Token FindIdent(Position cursor)
        {
            if (afterIdent || afterValue)
            {
                compiler.AddMessage(cursor.Copy(), new Message()
                {
                    IsError = true,
                    Text = "Unexpected identifier after identifier or value"
                });
                cursor++;
                afterIdent = afterValue = false;
                return null;
            }

            Position start = cursor.Copy();
            List<char> attr = new List<char>();

            while (cursor.IsLetterOrDigit)
            {
                attr.Add((char)cursor.Current);

                cursor++;
            }
            Lexem tag = getLexemByValue(program.Substring(start.Index, cursor.Index - start.Index).ToLower());
            afterIdent = tag == Lexem.IDENT;
            afterValue = false;

            if (tag == Lexem.TYPEDEF)
                typedef = true;

            if(typedef && afterIdent)
            {
                listTypedef.Add(program.Substring(start.Index, cursor.Index - start.Index).ToLower());
                typedef = false;
            }

            if (afterIdent) return new IdentToken()
            {
                Code = compiler.AddName(new String(attr.ToArray())),
                Coords = new Fragment() { Start = start, End = cursor.Copy() }
            };
            else return new SpecToken()
            {
                Tag = tag,
                Coords = new Fragment() { Start = start, End = cursor.Copy() }
            };
        }

        private Token FindNumber(Position cursor)
        {
            if (afterIdent || afterValue)
            {
                compiler.AddMessage(cursor.Copy(), new Message()
                {
                    IsError = true,
                    Text = "Unexpected identifier after identifier or value"
                });
                cursor++;
                afterIdent = afterValue = false;
                return null;
            }

            Position start = cursor.Copy();
            List<char> attr = new List<char>();
            int last = cursor.Current;
            while (cursor.IsDecimalDigit
                || cursor.Current == '.'
                || cursor.Current == '+'
                || cursor.Current == '-'
                || cursor.Current == 'E'
                || cursor.Current == 'e')
            {
                if ((!last.Equals('E') || !last.Equals('e')) &&
                    (cursor.Current.Equals('+') || cursor.Current.Equals('-')))
                    break;
                attr.Add(cursor.Current == '.' ? ',' : (char)cursor.Current);
                last = cursor.Current;
                cursor++;
            }
            
            afterIdent = false;


            string value = new String(attr.ToArray());

            long iValue;
            double fValue;
            if (long.TryParse(value, out iValue)) return new INumberToken()
            {
                Coords = new Fragment() { Start = start, End = cursor.Copy() },
                Value = iValue
            };
            else if (double.TryParse(value, out fValue)) return new FNumberToken()
            {
                Coords = new Fragment() { Start = start, End = cursor.Copy() },
                Value = fValue
            };


            string errorText = afterValue ? "Constant is too large: " + value : "Bad constant";
            compiler.AddMessage(start, new Message()
            {
                IsError = true,
                Text = errorText
            });

            return null;
        }

        private Token FindStr(Position cursor)
        {
            cursor++;

            if (afterIdent || afterValue)
            {
                compiler.AddMessage(cursor.Copy(), new Message()
                {
                    IsError = true,
                    Text = "Unexpected value after identifier or value"
                });
                afterIdent = afterValue = false;
                return null;
            }

            Position start = cursor.Copy();
            List<char> attr = new List<char>();
            bool correctStr = false;

            while (cursor.Current != -1 && !cursor.IsNewLine)
            {
                if (cursor.Current == '\"')
                {
                    correctStr = true;
                    break;
                }
                else if (cursor.Current == '\\')
                {
                    char? escape = TryParseEscape(cursor);
                    if (escape == null)
                    {
                        compiler.AddMessage(cursor.Copy(), new Message()
                        {
                            IsError = true,
                            Text = "Bad escape \\" + (char)cursor.Current
                        });
                        attr.Add('\\');
                        attr.Add((char)cursor++.Current);
                    }
                    else
                        attr.Add((char)escape);
                }
                else
                {
                    attr.Add((char)cursor.Current);
                    cursor++;
                }
            }
            afterValue = correctStr;
            afterIdent = false;


            if (!correctStr)
            {
                compiler.AddMessage(cursor++.Copy(), new Message()
                {
                    IsError = true,
                    Text = "\" expected"
                });

                return null;
            }

            return new StringToken()
            {
                Value = new string(attr.ToArray()),
                Coords = new Fragment() { Start = start, End = cursor++.Copy() }
            };
        }

        private Token FindChar(Position cursor)
        {
            cursor++;
            if (afterIdent || afterValue)
            {
                compiler.AddMessage(cursor.Copy(), new Message()
                {
                    IsError = true,
                    Text = "Unexpected value after identifier or value"
                });
                afterIdent = afterValue = false;
                return null;
            }

            /*
             * Первый символ или escape-последовательность
             */
            char? codePoint = null;

            if (cursor.Current == '\\')
            {
                codePoint = TryParseEscape(cursor);
            }
            else if (!cursor.IsNewLine)
            {
                codePoint = (char)cursor.Current;
                cursor++;
            }


            /*
             * Закрывающаяся кавычка
             * Итог
             */
            afterIdent = false;
            afterValue = codePoint != null && cursor.Current == '\'';

            if (afterValue) return new INumberToken()
            {
                Coords = new Fragment() { Start = cursor++.Copy() },
                Value = (char)codePoint
            };


            string errorText = cursor.Current != '\'' ? "\' expected" : "Bad escape";
            compiler.AddMessage(cursor++, new Message()
            {
                IsError = true,
                Text = errorText
            });

            return null;
        }

        private char? TryParseEscape(Position cursor)
        {
            char? result = null;
            if (cursor.Current != '\\')
            {
                return null;
            }
            switch ((++cursor).Current)
            {
                case '\'':
                case '\"':
                case '\\':
                    result = (char)cursor.Current;
                    break;
                case 'a':
                    result = '\a';
                    break;
                case 'b':
                    result = '\b';
                    break;
                case 'f':
                    result = '\f';
                    break;
                case 'n':
                    result = '\n';
                    break;
                case 'r':
                    result = '\r';
                    break;
                case 't':
                    result = '\t';
                    break;
                case 'v':
                    result = '\v';
                    break;
                default:
                    break;
            }
            cursor++;
            return result;
        }

        private Token FindOther(Position cursor)
        {
            Lexem tag = Lexem.ERROR;
            Position start = cursor.Copy();
            switch (cursor.Current)
            {
                case '>':
                    if ((++cursor).Current == '>')
                        if ((++cursor).Current == '=')
                        {
                            tag = Lexem.ASSIGN_RIGHT;
                            cursor++;
                        }
                        else
                            tag = Lexem.OP_RIGHT;
                    else if (cursor.Current == '=')
                    {
                        tag = Lexem.LOGIC_NOT_S;
                        cursor++;
                    }
                    else
                        tag = Lexem.LOGIC_L;
                    break;
                case '<':
                    if ((++cursor).Current == '<')
                        if ((++cursor).Current == '=')
                        {
                            tag = Lexem.ASSIGN_LEFT;
                            cursor++;
                        }
                        else
                            tag = Lexem.OP_LEFT;
                    else if (cursor.Current == '=')
                    {
                        tag = Lexem.LOGIC_NOT_L;
                        cursor++;
                    }
                    else
                        tag = Lexem.LOGIC_S;
                    break;
                case '+':
                    if ((++cursor).Current == '+')
                        if ((++cursor).Current == '+')
                            if (afterIdent)
                            {
                                tag = Lexem.INC;
                                cursor++;
                                bufToken = new SpecToken()
                                {
                                    Tag = Lexem.OP_ADD,
                                    Coords = new Fragment() { Start = cursor++ }
                                };
                            }
                            else
                                tag = Lexem.INC;
                        else
                            tag = Lexem.INC;
                    else if (cursor.Current == '=')
                    {
                        tag = Lexem.ASSIGN_ADD;
                        cursor++;
                    }
                    else
                        tag = Lexem.OP_ADD;
                    break;
                case '-':
                    if ((++cursor).Current == '-')
                        if ((++cursor).Current == '-')
                            if (afterIdent)
                            {
                                tag = Lexem.DEC;
                                cursor++;
                                bufToken = new SpecToken()
                                {
                                    Tag = Lexem.OP_SUB,
                                    Coords = new Fragment() { Start = cursor++ }
                                };
                            }
                            else
                                tag = Lexem.DEC;
                        else
                            tag = Lexem.DEC;
                    else if (cursor.Current == '=')
                    {
                        tag = Lexem.ASSIGN_SUB;
                        cursor++;
                    }
                    else if (cursor.Current == '>')
                    {
                        tag = Lexem.PTR_LINK;
                        cursor++;
                    }
                    else
                        tag = Lexem.OP_SUB;
                    break;
                case '*':
                    if ((++cursor).Current == '=')
                    {
                        tag = Lexem.ASSIGN_MUL;
                        cursor++;
                    }
                    else
                        tag = Lexem.OP_MUL;
                    break;
				case '/':
				{
					if ((++cursor).Current == '=') {
						tag = Lexem.ASSIGN_DIV;
						cursor++;
					} else if(cursor.Current == '*')
					{
						cursor++;
						do {
							do
								cursor++;
							while(cursor.Current != '*' && cursor.Current != -1);
							cursor++;
						} while(cursor.Current != '/' && cursor.Current != -1);
						if (cursor.Current == -1) {
							Console.WriteLine ("End of programm found, */ expected");	
							Environment.Exit( 0 );
						}
						comments.Add (new Fragment() {Start = start, End = cursor.Copy() });
						cursor++;
						afterIdent = afterValue = false;
						return scan (cursor);
					} else if(cursor.Current == '/') 
					{
						while(cursor.Current != 13 && cursor.Current != -1){
							var t = new Fragment () { Start = start, End = cursor.Copy () };
							Console.Write (cursor.Current  + "    ");
							Console.WriteLine(t.ToString());	
							cursor++;
						}

						comments.Add (new Fragment () {Start = start, End = cursor.Copy()});
						cursor++;
						afterIdent = afterValue = false;
						return scan (cursor);
					}
					else
						tag = Lexem.OP_DIV;
					break;
				}
                case '%':
                    if ((++cursor).Current == '=')
                    {
                        tag = Lexem.ASSIGN_PROCENT;
                        cursor++;
                    }
                    else
                        tag = Lexem.OP_PROCENT;
                    break;
                case '&':
                    if ((++cursor).Current == '=')
                    {
                        tag = Lexem.ASSIGN_AND;
                        cursor++;
                    }
                    else if (cursor.Current == '&')
                    {
                        tag = Lexem.LOGIC_AND;
                        cursor++;
                    }
                    else
                        tag = Lexem.OP_AND;
                    break;
                case '^':
                    if ((++cursor).Current == '=')
                    {
                        tag = Lexem.ASSIGN_XOR;
                        cursor++;
                    }
                    else
                        tag = Lexem.OP_XOR;
                    break;
                case '|':
                    if ((++cursor).Current == '=')
                    {
                        tag = Lexem.ASSIGN_OR;
                        cursor++;
                    }
                    else if (cursor.Current == '|')
                    {
                        tag = Lexem.LOGIC_OR;
                        cursor++;
                    }
                    else
                        tag = Lexem.OP_OR;
                    break;
                case '!':
                    if ((++cursor).Current == '=')
                    {
                        tag = Lexem.LOGIC_NOT_EQ;
                        cursor++;
                    }
                    else
                        tag = Lexem.OP_NOT;
                    break;
                case '=':
                    if ((++cursor).Current == '=')
                    {
                        tag = Lexem.LOGIC_EQ;
                        cursor++;
                    }
                    else
                        tag = Lexem.ASSIGN;
                    break;
                case '[':
                    tag = Lexem.PTR_MASS_LEFT;
                    cursor++;
                    break;
                case ']':
                    tag = Lexem.PTR_MASS_RIGHT;
                    cursor++;
                    break;
                case '{':
                    tag = Lexem.BLOCK_LEFT;
                    cursor++;
                    break;
                case '}':
                    tag = Lexem.BLOCK_RIGHT;
                    cursor++;
                    break;
                case '(':
                    tag = Lexem.FUNC_ARG_LEFT;
                    cursor++;
                    break;
                case ')':
                    tag = Lexem.FUNC_ARG_RIGHT;
                    cursor++;
                    break;
                case ';':
                    tag = Lexem.SEMICOLON;
                    cursor++;
                    break;
                case ',':
                    tag = Lexem.COMMA;
                    cursor++;
                    break;
                case '.':
                    tag = Lexem.POINT;
                    cursor++;
                    break;
                case ':':
                    tag = Lexem.DOUBLE_POINT;
                    cursor++;
                    break;
                default:
                    break;
            }
            afterIdent = afterValue = false;

            if (tag == Lexem.ERROR)
            {
                compiler.AddMessage(cursor.Copy(), new Message()
                {
                    IsError = true,
                    Text = "Unexpected character " + (char)cursor.Current
                });
                cursor++;
                return null;
            }
            return new SpecToken()
            {
                Tag = tag,
                Coords = new Fragment() { Start = start }
            };
        }
        #endregion

		public Lexer(COptions opts, Compiler compiler, string progr)
        {
            this.opts = opts;
            this.compiler = compiler;

            this.program = progr;
        }

        public IEnumerable<Token> Tokens()
        {
            Position cursor = new Position(program);
            Token t = new SpecToken()
            {
                Tag = Lexem.START,
                Coords = new Fragment() { Start = cursor }
            };

            if (COptions.DumpLexer)
            {
                Console.WriteLine(Compiler.PHASE_SEPARATOR);
                Console.WriteLine(t.ToString());
            }
            yield return t;

            while (t.Tag != Lexem.END)
            {
                t = scan(cursor);
				if (COptions.DumpLexer) {
					Console.WriteLine (t.ToString ());
				}
                yield return t;
            }
        }

        public List<Token> AllTokens()
        {
            Position cursor = new Position(program);
            List<Token> tokens = new List<Token>();

            tokens.Add(
                new SpecToken()
                {
                    Tag = Lexem.START,
                    Coords = new Fragment() { Start = cursor.Copy() }
                }
            );

            while (tokens.Last().Tag != Lexem.END)
                tokens.Add(scan(cursor));

            return tokens;
        }
    }

    public class CDirAdjListGraph<
                    EProperties,
                    VProperties>
        : CDirGraph<EProperties, VProperties>
    {
        public CDirAdjListGraph()
            : base(new CDirectedAdjListStorage<EProperties, VProperties>())
        { }
    }

    class CAstEdge { }

    public class SyntaxTree
    {
        private CDirAdjListGraph<ParseTreeNode, CAstEdge> tree;

        public SyntaxTree()
        {
            tree = new CDirAdjListGraph<ParseTreeNode, CAstEdge>();
        }

    }
    

    public class ParseTreeNode
    {
        public virtual Tags Tag { get; set; }

        public virtual string Text { get; set; }

        // родительский узел для данного узла дерева
        private ParseTreeNode parent = null;
        
        // потомки (ветви) данного узла дерева
        private IList<ParseTreeNode> childs = new List<ParseTreeNode>();
        
		public ParseTreeNode(Lexem tag, string text)
        {
			try {
				text = text.Split(':')[0];
				text = text.Split('[')[0];
			}catch (Exception) {
				
			}
			Tag = NodeType.getNodeType(text);
            Text = text;
        }
        
        // метод добавления дочернего узла
        public void AddChild(ParseTreeNode child)
        {
            if (child.Parent != null)
            {
                child.Parent.childs.Remove(child);
            }
            childs.Remove(child);
            childs.Add(child);
            child.parent = this;
        }
        
        // метод удаления дочернего узла
        public void RemoveChild(ParseTreeNode child)
        {
            childs.Remove(child);
            if (child.parent == this)
                child.parent = null;
        }

        // метод получения дочернего узла по индексу
        public ParseTreeNode GetChild(int index)
        {
            return childs[index];
        }
        
      
        public int ChildCount
        {
            get
            {
                return childs.Count;
            }
        }
        
        // родительский узел (свойство)
        public ParseTreeNode Parent
        {
            get
            {
                return parent;
            }
            set
            {
                value.AddChild(this);
            }
        }
        
        // индекс данного узла в дочерних узлах родительского узла
        public int Index
        {
            get
            {
                return Parent == null ? -1
                    : Parent.childs.IndexOf(this);
            }
        }

        public void Print(int level)
        {
			string temp = String.Empty;
			for (int i = 0; i < level; i++) 
				Console.Write ("|\t");
			
		
            Console.WriteLine(">" + Text);
            if (childs != null)
                for(int i = 0; i < ChildCount; i++)
                {
                    childs[i].Print(level + 1);
                }
        }

		public AST buildAST()
		{
			AST ast = new AST (Text, Tag);
			if (childs != null)
				for(int i = 0; i < ChildCount; i++)
				{
					var newAst = new AST (GetChild(i).Text, GetChild(i).Tag, ast);
					ast.AddChild(newAst);
					GetChild(i).build (newAst);
				}
			return ast;
		}

		private void build(AST ast) 
		{
			if (childs != null)
				for(int i = 0; i < ChildCount; i++)
				{
					var newAst = new AST (GetChild(i).Text, GetChild(i).Tag, ast);
					ast.AddChild(newAst);
					GetChild(i).build (newAst);
				}
		}

    }

	public enum Tags
	{
		KEYWORD = 0, 
		NONTERMINAL = 1, 
		IDENT = 2, 
		COLOMNS = 3, 
		BRACKETS = 4
	}

    public class NodeType
    {
		public static Tags getNodeType(String text)
        {
            switch(text)
            {
				case "IDENT":
					return Tags.IDENT;
				case "INT_CONST":
					return Tags.KEYWORD;
				case "FLOAT_CONST":
					return Tags.KEYWORD;
				case "CHAR":
					return Tags.KEYWORD;
				case "DOUBLE":
					return Tags.KEYWORD;
				case "FLOAT":
					return Tags.KEYWORD;
				case "INT":
					return Tags.KEYWORD;
				case "translation_unit":
					return Tags.KEYWORD;
				case "LONG":
					return Tags.KEYWORD;
				case "SHORT":
					return Tags.KEYWORD;
				case "VOID":
					return Tags.KEYWORD;
				case "BYTE":
				    return Tags.KEYWORD;
				case "CONST":
					return Tags.KEYWORD;
				case "SIGNED":
					return Tags.KEYWORD;
				case "UNSIGNED":
					return Tags.KEYWORD;
				case "IF":
					return Tags.KEYWORD;
				case "ELSE":
					return Tags.KEYWORD;
				case "SWITCH":
					return Tags.KEYWORD;
				case "CASE":
					return Tags.KEYWORD;
				case "DEFAULT":
					return Tags.KEYWORD;
				case "FOR":
					return Tags.KEYWORD;
				case "DO":
					return Tags.KEYWORD;
				case "WHILE":
					return Tags.KEYWORD;
				case "BREAK":
					return Tags.KEYWORD;
				case "CONTINUE":
					return Tags.KEYWORD;
				case "RETURN":
					return Tags.KEYWORD;
				case "OP_ADD":
					return Tags.KEYWORD;
				case "OP_SUB":
					return Tags.KEYWORD;
				case "OP_DIV":
					return Tags.KEYWORD;
				case "OP_MUL":
					return Tags.KEYWORD;
				case "OP_PROCENT":
					return Tags.KEYWORD;
				case "ASSIGN":
					return Tags.KEYWORD;
				case "ASSIGN_ADD":
					return Tags.KEYWORD;
				case "ASSIGN_SUB":
					return Tags.KEYWORD;
				case "ASSIGN_MUL":
					return Tags.KEYWORD;
				case "ASSIGN_DIV":
					return Tags.KEYWORD;
				case "ASSIGN_PROCENT":
					return Tags.KEYWORD;
				case "ASSIGN_AND":
					return Tags.KEYWORD;
				case "ASSIGN_OR":
					return Tags.KEYWORD;
				case "ASSIGN_XOR":
					return Tags.KEYWORD;
                case "STRING_LITERAL":
                    return Tags.KEYWORD;
                case "ASSIGN_RIGHT":
					return Tags.KEYWORD;
				case "ASSIGN_LEFT":
					return Tags.KEYWORD;
				case "INC":
					return Tags.KEYWORD;
				case "DEC":
					return Tags.KEYWORD;
				case "OP_AND":
					return Tags.KEYWORD;
				case "OP_OR":
					return Tags.KEYWORD;
				case "OP_XOR":
					return Tags.KEYWORD;
				case "OP_RIGHT":
					return Tags.KEYWORD;
				case "OP_LEFT":
					return Tags.KEYWORD;
				case "OP_NOT":
					return Tags.KEYWORD;
				case "LOGIC_EQ":
					return Tags.KEYWORD;
				case "LOGIC_NOT_EQ":
					return Tags.KEYWORD;
				case "LOGIC_NOT_S":
					return Tags.KEYWORD;
				case "LOGIC_NOT_L":
					return Tags.KEYWORD;
				case "LOGIC_AND":
					return Tags.KEYWORD;
				case "LOGIC_OR":
					return Tags.KEYWORD;
				case "LOGIC_S":
					return Tags.KEYWORD;
				case "LOGIC_L":
					return Tags.KEYWORD;
				case "BLOCK_LEFT":
					return Tags.BRACKETS;
				case "BLOCK_RIGHT":
					return Tags.BRACKETS;
				case "FUNC_ARG_LEFT":
					return Tags.BRACKETS;
				case "FUNC_ARG_RIGHT":
					return Tags.BRACKETS;
				case "SEMICOLON":
					return Tags.COLOMNS;
				case "COMMA":
					return Tags.COLOMNS;

				case "PTR_MASS_LEFT":
					return Tags.BRACKETS;
				case "PTR_MASS_RIGHT":
					return Tags.BRACKETS;
				case "PTR_LINK":
					return Tags.KEYWORD;
				case "POINT":
					return Tags.KEYWORD;
				case "DOUBLE_POINT":
					return Tags.KEYWORD;
				case "TYPEDEF":
					return Tags.KEYWORD;
				case "NEWTYPE":
					return Tags.KEYWORD;
				case "primary_expression":
					return Tags.NONTERMINAL;
				case "postfix_expression":
					return Tags.NONTERMINAL;
				case "argument_expression_list":
					return Tags.NONTERMINAL;
				case "unary_expression":
					return Tags.NONTERMINAL;
				case "unary_operator":
					return Tags.NONTERMINAL;
				case "cast_expression":
					return Tags.NONTERMINAL;
				case "multiplicative_expression":
					return Tags.NONTERMINAL;
				case "additive_expression":
					return Tags.NONTERMINAL;
				case "shift_expression":
					return Tags.NONTERMINAL;
				case "relational_expression":
					return Tags.NONTERMINAL;
				case "equality_expression":
					return Tags.NONTERMINAL;
				case "and_expression":
					return Tags.NONTERMINAL;
				case "exclusive_or_expression":
					return Tags.NONTERMINAL;
				case "inclusive_or_expression":
					return Tags.NONTERMINAL;
				case "logical_and_expression":
					return Tags.NONTERMINAL;
				case "logical_or_expression":
					return Tags.NONTERMINAL;
				case "conditional_expression":
					return Tags.NONTERMINAL;
				case "assignment_expression":
					return Tags.NONTERMINAL;
				case "assignment_operator":
					return Tags.NONTERMINAL;
				case "expression":
					return Tags.NONTERMINAL;
				case "constant_expression":
					return Tags.NONTERMINAL;
				case "declaration":
					return Tags.NONTERMINAL;
				case "declaration_specifiers":
					return Tags.NONTERMINAL;
				case "init_declarator_list":
					return Tags.NONTERMINAL;
				case "init_declarator":
					return Tags.NONTERMINAL;
				case "type_specifier":
					return Tags.NONTERMINAL;
				case "specifier_qualifier_list":
					return Tags.NONTERMINAL;
				case "type_qualifier":
					return Tags.NONTERMINAL;
				case "declarator":
					return Tags.NONTERMINAL;
				case "direct_declarator":
					return Tags.NONTERMINAL;
				case "pointer":
					return Tags.NONTERMINAL;
				case "type_qualifier_list":
					return Tags.NONTERMINAL;
				case "parameter_type_list":
					return Tags.NONTERMINAL;
				case "parameter_list":
					return Tags.NONTERMINAL;
				case "parameter_declaration":
					return Tags.NONTERMINAL;
				case "identifier_list":
					return Tags.NONTERMINAL;
				case "type_name":
					return Tags.NONTERMINAL;
				case "abstract_declarator":
					return Tags.NONTERMINAL;
				case "direct_abstract_declarator":
					return Tags.NONTERMINAL;
				case "initializer":
					return Tags.NONTERMINAL;
				case "initializer_list":
					return Tags.NONTERMINAL;
				case "statement":
					return Tags.NONTERMINAL;
				case "matched_statement":
					return Tags.NONTERMINAL;
				case "open_statement":
					return Tags.NONTERMINAL;
				case "labeled_statement":
					return Tags.NONTERMINAL;
				case "compound_statement":
					return Tags.NONTERMINAL;
				case "declaration_list":
					return Tags.NONTERMINAL;
				case "statement_list":
					return Tags.NONTERMINAL;
				case "expression_statement":
					return Tags.NONTERMINAL;
				case "iteration_statement":
					return Tags.NONTERMINAL;
				case "jump_statement":
					return Tags.NONTERMINAL;
				case "external_declaration":
					return Tags.NONTERMINAL;
				case "function_definition":
					return Tags.NONTERMINAL;


			default: return Tags.NONTERMINAL;
            }
        }
    }

	public class AST 
	{
		private string text;
		private Tags tag;
		private AST parent = null;
		private IList<AST> childs = new List<AST>();
		private bool needNonterminal = true;

		public AST(String Text, Tags Tag)
		{
			this.text = Text;
			this.tag = Tag;
			this.parent = null;
		}

		public AST(String Text, Tags Tag, AST Parent)
		{
			this.text = Text;
			this.tag = Tag;
			this.parent = Parent;
		}

		public int ChildCount
		{
			get
			{
				return childs.Count;
			}
		}

		public void AddChild(AST child)
		{
			if (child.parent != null)
			{
				child.parent.childs.Remove(child);
			}
			childs.Remove(child);
			childs.Add(child);
			child.parent = this;
		}

		public AST GetChild(int index)
		{
			return childs[index];
		}

		public void Print(int level)
		{
			bool mark = false;
			int num = 0;
			if (tag == Tags.NONTERMINAL) {
				for (int i = 0; i < ChildCount; i++) {
					if (this.GetChild (i).tag == Tags.KEYWORD || this.GetChild (i).tag == Tags.IDENT) {
						mark = true;
						break;
					} 
					if(this.GetChild (i).tag != Tags.COLOMNS)
						num++;
				}

			} else
				mark = true;
			
			if (tag != Tags.COLOMNS && tag != Tags.BRACKETS && mark)
			{
				for (int i = 0; i < level; i++)
					Console.Write ("|\t");
				Console.WriteLine (">" + text);
			} else if (!mark && num == 1)
				level--;
			else if (num != 1 && !mark) 
			{
				for (int i = 0; i < level; i++) 
					Console.Write ("|\t");

				Console.WriteLine (">" + text);
			}
			
		
			if (childs != null)
				for(int i = 0; i < ChildCount; i++)
				{
					childs[i].Print(level + 1);
				}
		}
		
	}

/*
Конец создания AST дерева
    */

	class Parser
    {
        public void parse(IEnumerable<Token> program, String srcCode)
        {
            var tb = new TableBuilder(File.ReadAllText("c.grammar"));

            /*
             * Таблицы GoTo & Action
             */
            List<Rule> rules;
            Tuple<Actions, short, LRItem>[,] tableAction;
            short[,] tableGoto;
            tb.createTable(out rules, out tableAction, out tableGoto);
            
            /*
             * Initial
             */
            var resultStack = new Stack<ParseTreeNode>();
            var stackStates = new Stack<int>();
            var stackTokens = new Stack<Token>();

            var loop = true;
            var pos = program.GetEnumerator();
            var lexems = Enum.GetNames(typeof(Lexem));
            /*
             * Start Option
             */
            pos.MoveNext();
            stackStates.Push(0);

            if (pos.Current.Tag == Lexem.START)
            {
                stackTokens.Push(pos.Current);
                pos.MoveNext();
            }
            
            while (stackTokens.Count != 0 && loop)
            {
                var a = pos.Current;
                var q = stackStates.First();
                var currentAction = tableAction[q, (int)a.Tag];
                
                switch (currentAction != null ? currentAction.Item1 : Actions.ERROR)
                {
                    case Actions.SHIFT:
                        stackTokens.Push(a);
                        if (!pos.MoveNext())
                            throw new Exception("Не хватает символов!!!");
                        stackStates.Push(currentAction.Item2);
                        break;
                    case Actions.REDUCE:
                        foreach (var s in currentAction.Item3.var)
                            stackStates.Pop();
                        int newState = stackStates.First();

					    var newPush = tableGoto[newState, rules.FindIndex(x => x.name.Equals(tb.allSymbols.Keys.ElementAt(currentAction.Item3.holder)))];
                        stackStates.Push(newPush);
                        /*
                         * Добавление
                         */
                        
                        if (currentAction.Item3.var.Count == 1)
                        {
							if (lexems.Contains(tb.allSymbols.Keys.ElementAt(currentAction.Item3.var[0])))
                            {
                                var t = stackTokens.Pop().ToString();
                                resultStack.Push(new ParseTreeNode(a.Tag, t.Split(':')[0].Split('[')[0]));
                            }
                        }
                        else
                        {
                            var dauthers = new Stack<ParseTreeNode>();
                            var x = new List<short>(currentAction.Item3.var);
                            x.Reverse();
						    var temp = new ParseTreeNode(a.Tag, tb.allSymbols.Keys.ElementAt(currentAction.Item3.holder).Split(':')[0].Split('[')[0]);
							foreach (var tok in x) {
								if (lexems.Contains (tb.allSymbols.Keys.ElementAt (tok))) {
									var t = stackTokens.Pop ().ToString ();
									dauthers.Push (new ParseTreeNode (a.Tag, t.Split (':') [0].Split ('[') [0]));
								} else
									dauthers.Push (resultStack.Pop ());
							}
                            
							while(dauthers.Count != 0)
                                temp.AddChild(dauthers.Pop());
                            
                            resultStack.Push(temp);

                        }

                        break;
                    case Actions.ACCEPT:
                        loop = false;
                        break;
                    case Actions.ERROR:
                        Console.WriteLine("Syntax Error at " + pos.Current.Coords.ToString());
                        return ;
                }

            }
            if (pos.MoveNext())
                throw new Exception("Файл не закончился((");
            else
            {
                var tree = resultStack.Pop();
                tree.Print(0);

				Console.WriteLine ("\n\n\n\tAST:\n\n");

				var ast = tree.buildAST ();

				ast.Print (0);
            }
        }
    }


}