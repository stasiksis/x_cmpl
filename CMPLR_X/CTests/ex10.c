typedef int stas;

struct Book {
	char author[40];
	char title[80];
	stas year;
	stas pages;
};

void main()
{

  	Book b[100];
   	stas i, n;
   	FILE *fp;
    fp = fopen("books", "rb");

    n = fread( &b[0], sizeof(Book), 100, fp);
    fclose ( fp );

   	for ( i = 0; i < n; i ++ )
		b[i].year = 2002;
    fp = fopen("books", "wb");

    fwrite ( b, sizeof(Book), n, fp );

    fclose ( fp );

}