const N = 10;

void main()
{
	int i, A[N], min;
 // ввод массива A
	for(i = 0; i < N; i++)
		scanf("%d", &A[i]);
	min = A[0];
	for ( i = 1; i < N; i ++ )
		if ( A[i] < min )
			min = A[i];

	printf("\n Минимальный элемент %d", min);

}