const N = 10;
void main()
{
	int i, j, A[N], c;
// ввод массива A
	for ( i = 0; i < N-1; i ++ )
		for ( j = N-2; j >= i; j -- )
 			if ( A[j] > A[j+1] )
 			{
 				c = A[j]; A[j] = A[j+1];
 				A[j+1] = c;
 			}
	
	printf("\n Отсортированный массив:\n");
	for( i = 0; i < N; i ++ )
	printf("%d ", A[i]);
}