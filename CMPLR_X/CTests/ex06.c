int Simple ( int N );
//--- основная программа ---
void main()
{
	int N;
	printf ( "\nВведите целое число ");
	scanf ( "%d", &N );
	if ( Simple (N) )
 		printf ( "Число %d - простое\n", N );
	else printf ( "Число %d - составное\n", N );
	getch();
}

//--- функция ---
int Simple ( int N )
{
	for ( int i = 2; i*i <= N; i ++ )
		if ( N % i = = 0 ) return 0;
	return 1;
}