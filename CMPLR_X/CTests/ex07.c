int SumDigits ( int N )
{
	int d, sum = 0;
	while ( N != 0 )
  	{
  		d = N % 10;
  		sum = sum + d;
  		N = N / 10;
  	}
	return sum;
}

void main()
{
	int N, s;
	printf ( "\nВведите целое число ");
	scanf ( "%d", &N );
	s = SumDigits (N);
	printf ( "Сумма цифр числа %d равна %d\n", N, s );
	getch();
}