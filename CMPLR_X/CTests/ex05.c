void QuickSort ( int A[], int from, int to )
{
	int x, i, j, temp;
 	if ( from >= to ) return;
 	i = from;
 	j = to;
 	x = A[(from+to)/2];
 	while ( i <= j ) {
 		while ( A[i] < x ) i ++;
 			while ( A[j] > x ) j --;
 		if ( i <= j ) {
 			temp = A[i]; A[i] = A[j]; A[j] = temp;
 			i ++;
 			j --;
 		}
 	}
	QuickSort ( A, from, j );
	QuickSort ( A, i, to );
}